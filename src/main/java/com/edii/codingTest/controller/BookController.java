package com.edii.codingTest.controller;

import com.edii.codingTest.model.Book;
import com.edii.codingTest.model.GeneralResponse;
import com.edii.codingTest.model.Status;
import com.edii.codingTest.repository.BookRepository;
import java.util.List;
import java.util.Optional;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(name = "/api")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @GetMapping(value = "/books")
    public ResponseEntity<?> getBooks() {
        HttpStatus status = HttpStatus.OK;
        GeneralResponse response = new GeneralResponse();

        List<Book> data = bookRepository.findAll();
        try {
            if (data == null) {
                response.setStatus(Status.NOT_FOUND);
                status = HttpStatus.NOT_FOUND;
            }

            response.setStatus(Status.SUCCESS);
            response.setMessage("Data sukses diload");
            response.setData(data);
        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setMessage(ex.getMessage());
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(response, status);
    }

    @GetMapping(value = "/book/{id}")
    public ResponseEntity<?> getDataById(Long id) {
        HttpStatus status = HttpStatus.OK;
        GeneralResponse response = new GeneralResponse();

        Optional<Book> data = bookRepository.findById(id);

        try {
            if (data.isPresent()) {
                response.setStatus(Status.SUCCESS);
                response.setData(data);
            } else {
                response.setStatus(Status.NOT_FOUND);
                response.setMessage("Data Tidak ditemukan");
                status = HttpStatus.NOT_FOUND;
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setMessage(ex.getMessage());
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(response, status);


    }

    @PostMapping(value = "/add")
    public ResponseEntity<?> addBook(@RequestBody Book book) {
        JSONObject response = new JSONObject();
        Book i = new Book();
        i.setTitle(book.getTitle());
        i.setPublisher(book.getPublisher());
        i.setType(book.getType());
        i.setPrice(book.getPrice());


        response.put("status", "SUCCESS");
        response.put("data", i);
        bookRepository.save(i);

        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> editBook(@PathVariable Long id, @RequestBody Book book) {
        JSONObject response = new JSONObject();

        bookRepository.findById(id).map(i -> {
            i.setTitle(book.getTitle());
            i.setPublisher(book.getPublisher());
            i.setType(book.getType());
            i.setPrice(book.getPrice());

            response.put("status", "SUCCESS");
            response.put("data", i);


            return bookRepository.save(i);

        });

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping(name = "/{id}")
    public GeneralResponse deleteBook(@PathVariable Long id) {

        bookRepository.findById(id).map(i -> {
            bookRepository.delete(i);
            return null;
        });

        return null;
    }


}
