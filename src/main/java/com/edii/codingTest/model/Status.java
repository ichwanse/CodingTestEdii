package com.edii.codingTest.model;

public enum Status {
    SUCCESS,
    ERROR,
    NOT_FOUND
}
